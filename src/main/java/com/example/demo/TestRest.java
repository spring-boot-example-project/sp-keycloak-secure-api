package com.example.demo;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.RolesAllowed;
import java.util.LinkedHashMap;
import java.util.Map;

@RestController
@RequestMapping("/test")
public class TestRest {

    @RequestMapping(value = "/anonymous", method = RequestMethod.GET)
    public ResponseEntity getAnonymous() {
        return ResponseEntity.ok("Hello Anonymous");
    }

    @RequestMapping(value = "/user", method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @RolesAllowed("user")
    public ResponseEntity getUser() {
        Map<String,String> hi = new LinkedHashMap<>();
        hi.put("tag","hello user");
        return ResponseEntity.ok(hi);
    }

    @RequestMapping(value = "/admin", method = RequestMethod.GET)
    @RolesAllowed("admin")
    public String getAdmin() {
        return "Hello Admin";
    }

    @RequestMapping(value = "/all-user", method = RequestMethod.GET)
    public ResponseEntity getAllUser() {
        return ResponseEntity.ok("Hello All User");
    }

}